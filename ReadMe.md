# HipChat Raspberry Pi Bot
A rudimentary bot driven by [Robut](https://github.com/justinweiss/robut) that
connects to [HipChat](http://hipchat.com) and replies to a certain set of commands.

## Future plugins

### Productivity
* Gist creator
* Stock photo search
* [Teamwork](http://teamwork.com) reporter
* PHP / Sass / [your-language-here] function lookup
* Big Sea documentation search
* Font search (preferably TypeKit)
* Make select plugins inactive until specified time span

### Fun
* Spotify / Google Music
* Random kitten gif
* Beer
* Pinball scores

## Existing plugin modifications
* Randomize Google searches instead of pulling from the first one every time

## Custom plugins
* Google book search
* YouTube search

## In the works
* Gist creator
* [Teamwork](http://teamwork.com) reporter