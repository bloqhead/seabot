class Robut::Plugin::Hello
	include Robut::Plugin

	desc "hello - a simple hello"
	match /^hello$/, :sent_to_me => true do
		replies = [
			"Hello",
			"Hi",
			"Yo!",
			"Hey",
			"Salutations",
			"Hello there",
			"Hi there",
			"Yo dawg",
			"/me waves"
		]
		shuffledReplies = replies.shuffle()
		reply( shuffledReplies[0] )
	end
end