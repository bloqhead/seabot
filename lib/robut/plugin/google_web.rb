require 'google-search'

# Responds with the first google book search result matching a query.
class Robut::Plugin::GoogleWeb
	include Robut::Plugin

	desc "web search <query> - responds with the first item from a Google search for <query>"
	match /^search (.*)/, :sent_to_me => true do |query|
		search = Google::Search::Web.new(:query => query, :safe => :active).first

		if search
			reply "#{search.title} — #{search.uri}"
		else
			reply "Couldn't find a search result"
		end
	end
end
