require 'activegist'

class Robut::Plugin::Gist
	include Robut::Plugin

	# @todo Finish this plugin
	# Reference: http://www.rubydoc.info/gems/activegist/0.7.1

	# If user credentials are required, we can make a collective Big Sea account
	# ActiveGist::API.username = ""
	# ActiveGist::API.password = ""

	def usage
		"#{at_nick} gist <filename> <code> - makes a Github gist out of the <code> snippet you supply and names it <filename>"
	end

	match /^gist (.*)/, :sent_to_me => true do |query|
		gist = ActiveGist.new

		if gist
			reply
		else
			reply "Gist could not be created"
		end
	end
end