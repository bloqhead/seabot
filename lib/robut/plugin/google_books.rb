require 'google-search'

# Responds with the first google book search result matching a query.
class Robut::Plugin::GoogleBooks
	include Robut::Plugin

	desc "book <query> - responds with the first book from a Google Books search for <query>"
	match /^book (.*)/, :sent_to_me => true do |query|
		book = Google::Search::Book.new(:query => query, :safe => :active).first

		if book
			reply "#{book.title} (#{book.published_year}) — #{book.uri}"
		else
			reply "Couldn't find a book"
		end
	end
end
