require 'google-search'

# Responds with the first google video search result matching a query.
class Robut::Plugin::YouTube
	include Robut::Plugin

	desc "video <query> - responds with the first video from a Google Videos search for <query>"
	match /^video (.*)/, :sent_to_me => true do |query|
		video = Google::Search::Video.new(:query => query, :safe => :active).first

		if video
			reply "#{video.title} — #{video.uri}"
		else
			reply "Couldn't find a video"
		end
	end
end
