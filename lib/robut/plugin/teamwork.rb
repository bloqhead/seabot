require 'json'
require 'teamwork'

class Robut::Plugin::Teamwork
	include Robut::Plugin

	def usage
		"projects - returns a list of our current projects [proof of concept]"
	end

	# Authentication
	Teamwork.authenticate( 'gel188chess' )

	# JSON
	base_url = File.read( "https://bigsea.teamwork.com/projects.json" )
	item = JSON.parse( base_url )

	match /^projects/, :sent_to_me => true do |query|
		# parse the JSON
		reply( item['projects'] )
	end
end